package com.test.leonetwork.db

import android.content.Context
import androidx.room.Room

object DbBuilder {
    fun appDb(context: Context):AppDatabase
    {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java, "database-name"
        ).build()
    }
}