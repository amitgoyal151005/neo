package com.test.leonetwork.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.test.leonetwork.api.responce.UserItem

@Database(entities = [UserItem::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}