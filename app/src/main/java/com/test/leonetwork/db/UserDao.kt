package com.test.leonetwork.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.test.leonetwork.api.responce.UserItem
import io.reactivex.Single

@Dao
interface UserDao {
    @Query("SELECT * FROM user")
    fun getAll(): LiveData<List<UserItem>>

    @Query("SELECT * FROM user WHERE id=(:userId) ")
    fun getUser(userId :Int): Single<UserItem>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg todo: UserItem)

}