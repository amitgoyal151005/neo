package com.test.leonetwork.util

object Key {
    const val USER_NAME="user_name"
    const val USER_EMAIL="user_email"
    const val USER_ADDRESS="user_address"
    const val USER_IMAGE="user_image"
}