package com.test.leonetwork.view.home.userlist

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.test.leonetwork.R
import com.test.leonetwork.databinding.FragmentUserListBinding
import com.test.leonetwork.view.home.userlist.adapter.UserListAdapters
import com.test.leonetwork.view.userdetail.UserDetailActivity

class UserList : Fragment() {

    private lateinit var mBinding: FragmentUserListBinding

    private val model: HomeViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model.refreshUser()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_list, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setList()
    }

    private fun setList() {
        val adapter = UserListAdapters()
        adapter.onItemClick = { userId ->
            activity?.let{
                val intent = Intent (it, UserDetailActivity::class.java)
                intent.putExtra(UserDetailActivity.USER_ID,userId)
                it.startActivity(intent)
            }
        }
        mBinding.userList.adapter = adapter;
        model.userList().observe(viewLifecycleOwner, Observer { it
            adapter.setlist(it)
        })

    }


}