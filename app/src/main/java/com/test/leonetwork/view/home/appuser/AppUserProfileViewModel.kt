package com.test.leonetwork.view.home.appuser

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.AndroidViewModel
import com.test.leonetwork.R
import com.test.leonetwork.util.Key

class AppUserProfileViewModel(application: Application) : AndroidViewModel(application) {
    private final val PREFERENCE_NAME = "lioDataShared"
   private val sharedPreferences: SharedPreferences =
        application.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)

    public fun getUserName(): String? {
        return sharedPreferences?.getString(Key.USER_NAME,"")
    }
    public fun getUserAddress(): String? {
        return sharedPreferences?.getString(Key.USER_ADDRESS,"")
    }
    public fun getUserEmail(): String? {
        return sharedPreferences?.getString(Key.USER_EMAIL,"")
    }
    public fun getUserImg(): String? {
        return sharedPreferences?.getString(Key.USER_IMAGE,"")
    }

    companion object {
        @JvmStatic
        @BindingAdapter("imageUrl")
        fun loadImage(view: ImageView, uri: String) {
            if (!uri.isNullOrEmpty()){

               view.setImageURI(Uri.parse(uri))
            }
            else{
                view.setImageResource(R.drawable.ic_profile)
            }
        }
    }

    public fun setData(name: String, email: String, address: String, image: Uri?) {
        sharedPreferences.edit()
            .putString(Key.USER_NAME, name).apply()
        sharedPreferences.edit()
            .putString(Key.USER_ADDRESS, address).apply()
        sharedPreferences.edit()
            .putString(Key.USER_EMAIL, email).apply()
        sharedPreferences.edit()
            .putString(Key.USER_IMAGE, image?.toString() ?: "").apply()

    }


}