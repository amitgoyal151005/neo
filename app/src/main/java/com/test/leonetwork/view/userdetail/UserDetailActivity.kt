package com.test.leonetwork.view.userdetail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.test.leonetwork.R
import com.test.leonetwork.databinding.ActivityUserDetailBinding

class UserDetailActivity : AppCompatActivity() {
    private lateinit var mBinding: ActivityUserDetailBinding

    companion object {
        val USER_ID = "user_id"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_detail)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_detail)
        mBinding.lifecycleOwner = this;
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "User Detail"

        val model: UserDetailViewModel by viewModels()
        mBinding.userDetailViewModel = model;
        model.setUser(intent.getIntExtra(USER_ID, 0))


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.getItemId()) {
             android.R.id.home -> onBackPressed()

        }
        return super.onOptionsItemSelected(item)
    }
}