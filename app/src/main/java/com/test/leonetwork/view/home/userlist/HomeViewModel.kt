package com.test.leonetwork.view.home.userlist

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.test.leonetwork.api.ApiInterfaceBuilder
import com.test.leonetwork.api.responce.UserItem
import com.test.leonetwork.api.responce.Users
import com.test.leonetwork.db.DbBuilder
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class HomeViewModel(application: Application) : AndroidViewModel(application) {
    val userDao = DbBuilder.appDb(application.applicationContext).userDao();
    val apiInterface = ApiInterfaceBuilder.buildService();


    fun userList(): LiveData<List<UserItem>> {
        return userDao.getAll();
    }

    fun refreshUser() {
        apiInterface.getProfileList()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.newThread())
            .subscribe(object : Observer<Users> {
                override fun onNext(users: Users) {
                    users?.data?.forEach {
                        userDao.insert(it)
                    }
                }

                override fun onError(e: Throwable) {
                }

                override fun onComplete() {
                }

                override fun onSubscribe(d: Disposable) {
                }
            })
    }

}