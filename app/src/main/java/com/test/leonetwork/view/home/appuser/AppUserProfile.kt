package com.test.leonetwork.view.home.appuser

import android.content.ActivityNotFoundException
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import com.test.leonetwork.R
import com.test.leonetwork.databinding.FragmentAppUserProfileBinding

class AppUserProfile : Fragment() {

    private lateinit var mBinding: FragmentAppUserProfileBinding
    private val model: AppUserProfileViewModel by activityViewModels()
    private var imageUri: Uri? = null


    private val getContent =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            if (uri != null) {
                imageUri = uri
                mBinding.imageviewAccountProfile.setImageURI(uri)
            }

        }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_app_user_profile, container, false)
        mBinding.appUserProfileViewModel = model
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding.save.setOnClickListener {
            model.setData(
                mBinding.name.text.toString(),
                mBinding.email.text.toString(),
                mBinding.address.text.toString(),
                imageUri
            )
            Toast.makeText(activity, "Saved", Toast.LENGTH_SHORT).show()
        }
        mBinding.imageviewAccountProfile.setOnClickListener { dispatchTakePictureIntent() }
    }

    private fun dispatchTakePictureIntent() {
        try {
            getContent.launch("image/*")
        } catch (e: ActivityNotFoundException) {
            // display error state to the user
        }
    }

}