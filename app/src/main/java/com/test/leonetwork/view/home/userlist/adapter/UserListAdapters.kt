package com.test.leonetwork.view.home.userlist.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.*
import com.bumptech.glide.Glide
import com.test.leonetwork.R
import com.test.leonetwork.api.responce.UserItem

class UserListAdapters : Adapter<UserListAdapters.MyViewHolder>() {

    var userItemList: List<UserItem> = listOf()

    var onItemClick: ((Int)->Unit) ?= null


    fun setlist(userItemList: List<UserItem>) {
        this.userItemList = userItemList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.user_card, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val userItem=userItemList.get(position);
        holder.itemView.setOnClickListener(View.OnClickListener { onItemClick?.invoke(userItem.id)})
        holder.nameTextView.text = userItem.first_name
        holder.emailTextView.text = userItem.email
        Glide
            .with(holder.userImageView)
            .load(userItem.avatar)
            .centerCrop()
            .placeholder(R.drawable.ic_profile)
            .into(holder.userImageView);
    }

    override fun getItemCount(): Int {
        return userItemList.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameTextView: TextView = itemView.findViewById(R.id.name);
        val emailTextView: TextView = itemView.findViewById(R.id.email);
        val userImageView: ImageView = itemView.findViewById(R.id.user_iv);
    }


}