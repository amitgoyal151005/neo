package com.test.leonetwork.view.userdetail

import android.app.Application
import android.util.Log
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.Glide
import com.test.leonetwork.R
import com.test.leonetwork.api.responce.UserItem
import com.test.leonetwork.db.DbBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class UserDetailViewModel(application: Application) : AndroidViewModel(application) {
    val userDao = DbBuilder.appDb(application.applicationContext).userDao()
    val imageUrl = MutableLiveData<String>("")


    val userItemLive: MutableLiveData<UserItem>? = MutableLiveData()
    companion object {
        @JvmStatic
        @BindingAdapter("imageUrl")
        fun loadImage(view: ImageView, url: String) {
            if (!url.isNullOrEmpty()){

                Glide.with(view.context).load(url).centerCrop()
                    .placeholder(R.drawable.ic_profile)
                    .into(view)
            }
            else{
                view.setImageResource(R.drawable.ic_profile)
            }
        }
    }

    fun setUser(userId: Int) {

        userDao.getUser(userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ value ->
                userItemLive?.postValue(value)
                imageUrl.postValue(value.avatar)
            }, { err ->
                Log.e("Error", "" + err)
            })
    }
}