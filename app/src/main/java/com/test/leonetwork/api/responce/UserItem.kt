package com.test.leonetwork.api.responce

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "user")
data class UserItem (

	@PrimaryKey @SerializedName("id") val id : Int,
	@ColumnInfo(name = "email") @SerializedName("email") val email : String,
	@ColumnInfo(name = "first_name") @SerializedName("first_name") val first_name : String,
	@ColumnInfo(name = "last_name") @SerializedName("last_name") val last_name : String,
	@ColumnInfo(name = "avatar") @SerializedName("avatar") val avatar : String
)