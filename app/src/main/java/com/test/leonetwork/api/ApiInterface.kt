package com.test.leonetwork.api

import com.test.leonetwork.api.responce.Users
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET

interface ApiInterface {

    @GET("/api/users")
    fun getProfileList(): Observable<Users>

}